import { Container } from "../components/container";

export default function Home() {
  return (
    <div className="Home">
      <title>Atlan - Query Viewer</title>
      <meta
        name="description"
        content="This is a web app, using which user can visualize the response based on
        the query provided."
      />

      <Container />
    </div>
  );
}
