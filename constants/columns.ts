export const COLUMNS: Record<string, string> = {
  TITLE: "TITLE",
  BRAND: "BRAND",
  CATEGORY: "CATEGORY",
  STOCK: "STOCK",
  DESCRIPTION: "DESCRIPTION",
  PRICE: "PRICE",
};

export const COLUMN_VS_COLUMN_TITLE: Record<string, string> = {
  [COLUMNS.TITLE]: "Title",
  [COLUMNS.BRAND]: "Brand",
  [COLUMNS.CATEGORY]: "Category",
  [COLUMNS.STOCK]: "Stocks",
  [COLUMNS.DESCRIPTION]: "Description",
  [COLUMNS.PRICE]: "Price",
};
