### [VIDEO DESCRIPTION HERE. PLEASE CHECK.](https://drive.google.com/file/d/1TFko-ZTabKpFOWzY209viBG8Baf_j0qN/view?usp=drivesdk)


## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

Deployed Project Link : [Vercel-Deployed-Link](https://table-eta-ten.vercel.app/)

## Javascript Framework & Packages used for this project.
1. [NextJS](https://nextjs.org/)
- Server-Side Rendering (SSR): Next.js provides built-in support for SSR, which significantly improves page loading speed and enhances SEO. By rendering pages on the server, it ensures that content is readily available to search engines and users.
- Static Site Generation (SSG): With Next.js, you can generate static pages during build time. This approach results in faster page loads and better performance. It’s especially useful for content-heavy websites and blogs.
- We as a developer don't have to think much about the routing, as it is already taken care by the NextJS

2. [React-Virtual](https://tanstack.com/virtual/latest)
- Virtualize only the visible DOM nodes within massive scrollable elements at 60FPS in TS/JS, React, Vue, Solid & Svelte while retaining 100% control over markup and styles.

## Initial page load time.
Page load time metrics.
Have used Lighthouse to get this metric.
| FCP | LCP |
| ------ | ------ |
|     0.4Sec   |   0.4Sec     |


