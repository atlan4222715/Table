// libs
import { useState, useCallback } from "react";

export const useTableData = (): {
  fetchTableData: (columns: string[]) => Promise<void>;
  loading: boolean;
  data: Array<Record<string, string>>;
  error: Error | undefined;
} => {
  const [data, setData] = useState<Array<Record<string, string>>>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<Error>();

  const fetchTableData = useCallback(async (_columns: string[]) => {
    try {
      setLoading(true);

      const columns = _columns.map((col) => col.toLowerCase()).join(",");

      const response = await fetch(
        `https://dummyjson.com/products/search?limit=100&skip=0&select=${columns}&q=`
      );

      if (!response.ok) {
        throw "Error";
      }

      const json = await response.json();

      setData([...json.products]);

      setLoading(false);
    } catch (error) {
      setLoading(false);
      setError(error as Error);
    }
  }, []);

  return { fetchTableData, data, loading, error };
};
