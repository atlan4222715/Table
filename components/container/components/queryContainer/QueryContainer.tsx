// libs
import { useState, useCallback, ChangeEvent, KeyboardEvent } from "react";

// constants
import { COLUMNS } from "../../../../constants/columns";

export const QueryContainer = ({
  onFetchColumnData,
}: {
  onFetchColumnData: (cols: string[]) => Promise<void>;
}): JSX.Element => {
  const [inputVal, setInputVal] = useState("");

  const [warning, setWarning] = useState(false);

  const onChange = useCallback((event: ChangeEvent<HTMLInputElement>) => {
    setInputVal(event.target.value);

    setWarning(false);
  }, []);

  const onFetch = useCallback(() => {
    const columnsToFetch = inputVal
      .replace(/\s+/g, "")
      .split(",")
      .map((column) => column.toUpperCase())
      .filter((column) => COLUMNS[column]);

    if (!columnsToFetch.length) {
      setWarning(true);
      return;
    }

    onFetchColumnData(columnsToFetch);
  }, [inputVal, onFetchColumnData]);

  const onKeyDown = useCallback(
    (event: KeyboardEvent<HTMLDivElement>) => {
      if (event.key === "Enter") {
        onFetch();
      }
    },
    [onFetch]
  );

  return (
    <div
      style={{
        padding: "24px",
        display: "flex",
        flexDirection: "column",
        gap: "8px",
        alignItems: "center",
        borderRadius: "8px",
      }}
      onKeyDown={onKeyDown}
    >
      <div>Please add comma seperated field, which you wanna fetch</div>
      <div style={{ fontSize: "10px" }}>
        Available Columns (Title, Brand, Category, Stock, Price, Description)
      </div>
      <div style={{ display: "flex", gap: "16px" }}>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <input
            size={80}
            placeholder="Add Fields to Fetch"
            value={inputVal}
            onChange={onChange}
            style={{ borderColor: warning ? "red" : "black" }}
          />
          {warning ? (
            <div style={{ fontSize: "10px", color: "red" }}>
              Please enter only valid columns.
            </div>
          ) : null}
        </div>
        <button style={{ width: "48px", height: "24px" }} onClick={onFetch}>
          Fetch
        </button>
      </div>
    </div>
  );
};
