export const TableHeader = ({
  columns,
}: {
  columns: string[];
}): JSX.Element => {
  return (
    <div
      style={{
        display: "flex",
        position: "sticky",
        top: "0px",
        backgroundColor: "black",
        zIndex: 10,
      }}
    >
      {columns.map((column) => (
        <div
          key={column}
          style={{
            border: "1px solid black",
            backgroundColor: "black",
            padding: "8px",
            color: "white",
            margin: "1px",
            flexGrow: 1,
            flexShrink: 1,
            flexBasis: 0,
          }}
        >
          {column}
        </div>
      ))}
    </div>
  );
};
