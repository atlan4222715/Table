// libs
import { useMemo } from "react";

// constants
import { COLUMNS } from "../../../../../constants/columns";

export const TableColumn = ({
  columnData,
}: {
  columnData: Record<string, string>;
}): JSX.Element => {
  const columnDataKeys = useMemo(
    () =>
      Object.keys(columnData).filter(
        (dataKey) => COLUMNS[dataKey.toUpperCase()]
      ),
    [columnData]
  );

  return (
    <div style={{ display: "flex", width: "100%" }}>
      {columnDataKeys.map((columnKey) => (
        <div
          key={columnKey}
          style={{
            border: "1px solid black",
            padding: "8px",
            margin: "1px",
            flexGrow: 1,
            flexShrink: 1,
            flexBasis: 0,
            wordBreak: "break-all",
            overflow: "hidden",
            textOverflow: "ellipsis",
            whiteSpace: "nowrap",
            height: "20px",
          }}
        >
          {columnData[columnKey]}
        </div>
      ))}
    </div>
  );
};
