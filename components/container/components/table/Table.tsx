// libs
import { useMemo, useRef, useCallback } from "react";
import { useVirtualizer } from "@tanstack/react-virtual";

// components
import { TableHeader } from "./components/TableHeader";
import { TableColumn } from "./components/TableColumn";
import { EmptyState } from "./components/EmptyState";

// constants
import { COLUMN_VS_COLUMN_TITLE } from "../../../../constants/columns";

const estimateSize = () => 40;

export const Table = ({
  queryResult,
}: {
  queryResult: {
    data: Array<Record<string, string>>;
    loading: boolean;
    error: Error | undefined;
  };
}): JSX.Element => {
  const { data, loading, error } = queryResult;

  const containerRef = useRef<HTMLDivElement>(null);

  const rowVirtualizer = useVirtualizer({
    count: data?.length ?? 1,
    getScrollElement: () => containerRef.current,
    estimateSize: estimateSize,
  });

  const columns = useMemo(
    () =>
      data?.[0]
        ? Object.keys(data[0])
            ?.map((colKey) => COLUMN_VS_COLUMN_TITLE[colKey.toUpperCase()])
            .filter(Boolean)
        : [],
    [data]
  );

  console.log(
    containerRef.current?.getBoundingClientRect().height,
    rowVirtualizer.getVirtualItems(),
    "Height"
  );

  if (loading) {
    return <>Loding...</>;
  }
  if (error) {
    return <>Error Occurred {error?.message}</>;
  }

  return data?.length ? (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        maxHeight: "500px",
        overflowY: "auto",
        border: "1px solid black",
        margin: "1px",
        position: "relative",
      }}
      ref={containerRef}
    >
      <TableHeader columns={columns}></TableHeader>

      <div
        style={{
          height: `${rowVirtualizer.getTotalSize()}px`,
          width: "100%",
          position: "relative",
        }}
      >
        {rowVirtualizer.getVirtualItems().map((virtualItem) => (
          <div
            key={virtualItem.key}
            style={{
              position: "absolute",
              top: 0,
              left: 0,
              width: "100%",
              height: `${virtualItem.size}px`,
              transform: `translateY(${virtualItem.start}px)`,
            }}
          >
            <TableColumn columnData={data[virtualItem.index]}></TableColumn>
          </div>
        ))}
      </div>

      {/* {data?.map((datum) => (
        <TableColumn columnData={datum}></TableColumn>
      ))} */}
    </div>
  ) : (
    <EmptyState />
  );
};
