// components
import { Table } from "./components/table";
import { QueryContainer } from "./components/queryContainer";

// hooks
import { useTableData } from "./hooks/useTableData";

export const Container = (): JSX.Element => {
  const { fetchTableData, ...queryResult } = useTableData();

  return (
    <div
      style={{
        width: "100%",
        display: "flex",
        flexDirection: "column",
        gap: "16px",
        position: "relative",
      }}
    >
      <QueryContainer onFetchColumnData={fetchTableData} />
      <Table queryResult={queryResult} />
    </div>
  );
};
